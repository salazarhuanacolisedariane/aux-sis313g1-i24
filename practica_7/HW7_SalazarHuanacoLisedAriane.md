# _**PRÁCTICA N°7**_ 
***
**Univ.:** Lised Ariane Salazar Huanaco 

**Docente:** Ing. Jose David Mamani Figueroa 

**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

**Siglas:** SIS-313 G1 

**Fecha:** 23/04/2024
***
## PREGUNTAS:
**1.	Realice un video explicando lo siguiente:
a.	La creación de una carpeta en el disco local C: mediante comandos desde la terminal
b.	La forma de cambiar de directorios utilizando comandos
NOTA: El video solo deberá explicar la creación de UNA SOLA carpeta**

ver video:
https://drive.google.com/drive/folders/1k0bhnenpipP0iiKR1ltntcfJy87RDxkq?hl=es


**2.	Adjunte capturas de pantalla del mismo proceso, tres veces más.**

![Captura de pantalla (469)](https://github.com/Lisyst/tarea-difici-/assets/152350293/fbfec0dd-8e69-40bf-82e8-710293cbe409)

**3.	Que comandos se utilizó en las anteriores preguntas.**
### cd / 
### cd (nombre)/(nombre)/(nombre) 
### dir
### md 
**4.	Explique 5 comandos para ser utilizados en la terminal y su forma de usarse en Windows (Terminal o Powershell) y en Linux (bash).**
1. **dir** muestra una lista de todo lo que hay dentro de la dirección en la que estamos
2. **cd** (nombre)  cambia de directorio 
3. **cd ..** para regresar a un directorio 
4. **del**  para borrar una carpeta 
5. **tree** para ver las carpetas en forma de árbol 



