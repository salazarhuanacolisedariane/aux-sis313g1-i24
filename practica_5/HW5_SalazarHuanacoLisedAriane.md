# _**PRÁCTICA N°5**_ 
***
### 🍒------------**🌟CSS DINER🌟**-------------🍒
### _Complete los primeros 5 niveles y los últimos 10 igualmente_...................................... ✏️

### _**🌟!NIVELES COMPLETADOS¡:🌟**_
- [x]  [level 1]()
- [x]  [level 2]()
- [x]  [level 3]()
- [x]  [level 4]()
- [x]  [level 5]()
- [x]  [level 6]()
- [x]  [level 7]()
- [x]  [level 8]()
- [x]  [level 9]()
- [x]  [level 10]()
- [x]  [level 11]()
- [x]  [level 12]()
- [x]  [level 13]()
- [x]  [level 14]() 
- [x]  [level 15]()
- [x]  [level 16]()
- [x]  [level 17]()
- [x]  [level 18]()
- [x]  [level 19]()
- [x]  [level 20]()
- [x]  [level 21]()
- [x]  [level 22]()
- [x]  [level 23]()
- [x]  [level 24]()
- [x]  [level 25]()
- [x]  [level 26]()
- [x]  [level 27]()
- [x]  [level 28]()
- [x]  [level 29]()
- [x]  [level 30]()
- [x]  [level 31]()
- [x]  [level 32]()

***
_**LEVEL 1**_ 🍽️🍽️

_level completed_ ✅

![level1](https://github.com/Lisyst/tarea-difici-/assets/152350293/c8ac19c5-e17c-44d9-a7de-cc869c4f989b)
***
***
_**LEVEL 2**_ 🍽️🟥

_level completed_ ✅

![level2](https://github.com/Lisyst/tarea-difici-/assets/152350293/535dfa3c-d905-44d1-a963-78ad1b8f0547)
***
***
_**LEVEL 3**_ 🍽️🟥

_level completed_ ✅

![level3](https://github.com/Lisyst/tarea-difici-/assets/152350293/e4d04e25-5301-4a6f-aa2b-c34fd8658a52)
***

***
_**LEVEL 4**_ 🍽️🟥🍎

_level completed_ ✅

![level4](https://github.com/Lisyst/tarea-difici-/assets/152350293/4c2af061-d5aa-4198-af6f-cd4e333bd6da)
***

***
_**LEVEL 5**_ 🍽️🟥🍊🥒

_level completed_ ✅

![level5](https://github.com/Lisyst/tarea-difici-/assets/152350293/da943d54-f257-4aaa-ba7a-4f4467dbf3fc)
***

***
_**LEVEL 22**_ 🍽️🥒🍎

_level completed_ ✅

![level22](https://github.com/Lisyst/tarea-difici-/assets/152350293/7925a1f3-cb98-4c02-b79b-3aeb463930e4)
***

***
_**LEVEL 23**_ 🍽️🥒🍎

_level completed_ ✅

![level23](https://github.com/Lisyst/tarea-difici-/assets/152350293/1401dd69-da93-4422-8ed1-be84a9b185cd)
***

***
_**LEVEL 24**_ 🍊🥒🍎

_level completed_ ✅

![level24](https://github.com/Lisyst/tarea-difici-/assets/152350293/f8add614-7905-459b-9e2f-528d0492bcc8)
***

***
_**LEVEL 25**_ 🍽️🥒🟥

_level completed_ ✅

![level25](https://github.com/Lisyst/tarea-difici-/assets/152350293/7bb86cfc-f5ed-4620-aa07-2c9f46e1a282)
***

***
_**LEVEL 26**_ 🍽️🥒🍊🍎

_level completed_ ✅

![level26](https://github.com/Lisyst/tarea-difici-/assets/152350293/197f4ef0-8074-4f89-9e61-25b7555bea4c)
***

***
_**LEVEL 27**_ 🟥🥒🍊🍎📃

_level completed_ ✅

![level27](https://github.com/Lisyst/tarea-difici-/assets/152350293/52314a9d-4a4c-43cc-a010-263a2421e31c)
***

***
_**LEVEL 28**_ 🍽️🥒🍊📃🍎🟥

_level completed_ ✅

![level28](https://github.com/Lisyst/tarea-difici-/assets/152350293/f274ced5-6c5a-4ea0-b2a0-e70f74952bb1)
***

***
_**LEVEL 29**_ 📃🥒🍊🍎🟥

_level completed_ ✅

![level29](https://github.com/Lisyst/tarea-difici-/assets/152350293/72b4407e-b6d1-4565-9612-e5649404e74e)
***

***
_**LEVEL 30**_ 📃🥒🍽️🍊🍎🟥

_level completed_ ✅

![level30](https://github.com/Lisyst/tarea-difici-/assets/152350293/1bdff5cd-894d-4c92-8c86-dc2f367448c8)
***

***
_**LEVEL 31**_ 📃🥒🍊🍽️🍎🟥

_level completed_ ✅

![level31](https://github.com/Lisyst/tarea-difici-/assets/152350293/18843f94-86b0-4a5c-86e9-7d2ead56e76a)
***

***
_**LEVEL 32**_ 📃🥒🍊🍽️🍎🟥

_level completed_ ✅

![level32](https://github.com/Lisyst/tarea-difici-/assets/152350293/249318f9-365a-4350-8a37-9c868f37e53d)
***
***
