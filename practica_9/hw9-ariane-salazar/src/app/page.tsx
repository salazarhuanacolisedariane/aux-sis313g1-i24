import Image from "next/image";
import styles from "./page.module.css";
import Button from "./componets/Button";
import { Pokemon } from "./componets/Pokemon";
import { Video } from "./componets/Video";
import { Lista } from "./componets/Lista";

export default function Home() {
  return (
    <main>
      <div className="Contendedor-principal">
      <div className="btn">
        <div className="Caja-btn">
          <Button title="HOLA"></Button>
          <Button title="ADIOS" transparente></Button> 
        </div>
      </div>
      
      <div className="Contenedor-Pokemon">
        <div className="Caja-Pokemon">
          <div className="Pokemon1">
            <Pokemon NombrePokemon="Chikorita" ImgPokemon={"img/uno.jpg"} TipoPokemon="Planta"></Pokemon>
          </div>
          <div className="Pokemon2">
            <Pokemon NombrePokemon="Charmander" ImgPokemon={"img/dos.jpg"} TipoPokemon="Fuego"></Pokemon>
        </div>
        <div className="Pokemon3">
          <Pokemon NombrePokemon="Mew" ImgPokemon={"img/tres.jpg"} TipoPokemon="Psíquico"></Pokemon></div>
        </div>
      </div>

      <div>
        <div className="Videos">
          <div className="Caja-Videos">
            <div className="Video1"> 
              <Video TituloVideo="Lovely" AutorVideo="Billie Eilish" Video={"video/BillieEilish.mp4"}></Video>
            </div>
            <div className="Video2">
              <Video TituloVideo="Mockingbird" AutorVideo="Eminem" Video={"video/eminen.mp4"}></Video>
            </div>
            <div className="Video3">
              <Video TituloVideo="Gucci los paños" AutorVideo="Karol G" Video={"video/karolG.mp4"}></Video>
            </div>
            
          </div>
        </div>
      </div>


      <div className="Contenedor-Lista">
        <div className="cajita1">
        <Lista ImagenLista={"img/icono1.svg"} TituloLista="Free Plan" Lista1="Unlimited Bandwitch" Lista2="Encrypted Connection" Lista3="No Traffic Logs" Lista4="Works on All Devices" PrecioLista="Free/mo" TituloBtn="Select"></Lista>
        </div>

        <div className="cajita2">
        <Lista ImagenLista={"img/icono2.svg"} TituloLista="Standard Plan" Lista1="Unlimited Bandwitch" Lista2="Encrypted Connection" Lista3="No Traffic Logs" Lista4="Works on All Devices" PrecioLista="$9/mo" TituloBtn="Select"></Lista>
        </div>

        <div className="cajita3">
        <Lista ImagenLista={"img/icono3.svg"} TituloLista="Premium Plan" Lista1="Unlimited Bandwitch" Lista2="Encrypted Connection" Lista3="No Traffic Logs" Lista4="Works on All Devices" PrecioLista="$12/mo" TituloBtn="Select"></Lista>
        </div>
      </div>
      </div>


    </main>
  );
}
