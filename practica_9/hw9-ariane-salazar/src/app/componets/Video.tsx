export function Video(props:{TituloVideo:string, AutorVideo:string, Video:any }){
    return(
        <div className="videos-div">
            <h2>{props.TituloVideo}</h2>
            <p>{props.AutorVideo}</p>
            <video src={props.Video} controls></video>
        </div>
    )
}