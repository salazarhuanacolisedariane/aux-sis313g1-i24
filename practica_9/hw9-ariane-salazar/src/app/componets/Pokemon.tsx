export function Pokemon(props:{NombrePokemon:string, ImgPokemon:any, TipoPokemon:string }){
    return(
        <div className="Pokemon-div">
            <h2>{props.NombrePokemon}</h2>
            <img src={props.ImgPokemon} alt="" />
            <p>{props.TipoPokemon}</p>
        </div>
    )
}