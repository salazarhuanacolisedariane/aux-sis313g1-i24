export default function Button (props:{title:string, transparente?:boolean}) {
    return(
        props.transparente?
        <button className="btn btn-transparente">{props.title}</button>
        :
        <button className="btn btn-lila">{props.title}</button>

    )
}