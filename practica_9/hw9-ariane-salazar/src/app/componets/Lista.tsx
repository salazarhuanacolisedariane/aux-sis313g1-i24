export function Lista(props:{ImagenLista:any, TituloLista:string, Lista1:string, Lista2:string, Lista3:string, Lista4:string, PrecioLista:string, TituloBtn:string }){
    return(
        <div className="Lista-div">
            <img src={props.ImagenLista} alt="" />
            <h3>{props.TituloLista}</h3>
            <ul>
                <li><img src="img/ticket.svg" alt="" />{props.Lista1}</li>
                <li><img src="img/ticket.svg" alt="" />{props.Lista2}</li>
                <li><img src="img/ticket.svg" alt="" />{props.Lista3}</li>
                <li><img src="img/ticket.svg" alt="" />{props.Lista4}</li>
            </ul>
            <h5>{props.PrecioLista}</h5>
            <button>{props.TituloBtn}</button>
        </div>
    )
}