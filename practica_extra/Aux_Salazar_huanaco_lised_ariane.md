# **PRÁCTICA EXTRA** ✏️

_**Univ.:** Lised Ariane Salazar Huanaco_

_**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA_

_**Siglas:** SIS-313 G1_


### **CUENTA DE FIGMA**
![cuenta_figma](https://github.com/Lisyst/tarea-difici-/assets/152350293/a302e515-e6ba-44f1-893f-70def7fc8f35)


### **REPOSITORIO 1ER PARCIAL**
![repositorio_primerparcial](https://github.com/Lisyst/tarea-difici-/assets/152350293/8dbf4653-f735-4967-b64f-2c1d130cfccf)
