
# PRÁCTICA N° 3
***Completando los niveles (Medium)***

* *level 1-20*    **completado** 
* *level 21*     **completado** 
![level_21_medium](https://github.com/Lisyst/tarea-difici-/assets/152350293/8f8117ab-e74e-43ce-96d3-9cb61e3fd88e)
* *level 22*     **completado** 
![level_22_medium](https://github.com/Lisyst/tarea-difici-/assets/152350293/8feeb78a-6464-494c-ae65-f252c7ac3953)
* *Level 23*     **completado** 
![level_23_medium](https://github.com/Lisyst/tarea-difici-/assets/152350293/a0e305a0-a2ff-41ae-8d71-f522e6a51124)
* *Level 24*     **completado** 
![level_24_medium](https://github.com/Lisyst/tarea-difici-/assets/152350293/298bbab9-caca-4571-9dda-2195aa3854cd)





***Completando los niveles (Hard)***

* *level 1-20*    **completado** 
* *level 21*      **completado** 
![level_21_Hard](https://github.com/Lisyst/tarea-difici-/assets/152350293/0c75471e-570a-4e30-bf1d-49173ffcae2d)

* *level 22*      **completado** 
![level_22_Hard](https://github.com/Lisyst/tarea-difici-/assets/152350293/36b9fc07-6c23-4f09-98a9-86f5ee245edf)
* *Level 23*
![level_23_Hard](https://github.com/Lisyst/tarea-difici-/assets/152350293/e1ab320d-9d61-40ad-af11-c298030a2e76)

* *Level 24*     **completado** 
![level_24_Hard](https://github.com/Lisyst/tarea-difici-/assets/152350293/7c77eee2-b8eb-4cb0-b85a-fb2a8feb9332)

