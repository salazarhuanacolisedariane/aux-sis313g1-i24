import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Link from "next/link";
import { useRef } from "react";
import { Company } from "@/components/Company";

export default function SingUp() {
    return(
        <section className="login container">
            <div className="login-img">
                <img src="/images/singUp.svg"></img>
            </div>
            <div>
                <Logo/>
                <div>
                    <h1>Sing Up</h1>
                    <p>Let’s get you all st up so you can access your personal account.</p>
                </div>
                <div>
                    <div className="signUp-ls">
                        <div>
                            <p>First Name</p>
                            <input type="text" id="username" name="username" />
                            <p>Gmail</p>
                            <input type="text" id="username" name="username" />
                            </div>
                        <div>
                            <p>Last Name</p>
                            <input type="text" id="username" name="username" />
                            <p>Phone Number</p>
                            <input type="text" id="username" name="username" />
                        </div>
                    </div>
                    <div>
                        <p>password</p>
                        <input type="password" id="pass" name="password" />
                        <p>Confirm Password</p>
                        <input type="password" id="pass" name="password" />
                        <div className="Remember">
                            <img src="/images/icon-cuadrado.svg" alt="" />
                        <p>I agree to all the <span className="textorosa">Terms</span> and <span className="textoros">Privacy Policies</span> </p>
                        </div>
                    </div>
                    <div className="login-button">
                        <Button text="Create account"></Button>
                    </div>
                    <div className="parte-link2">
                        <p>Already have an account?</p>  
                        <Link href="/login">Login</Link>
                    </div> 
                    <div className="Orloginwith">
                        <p>Or Sign up with</p>
                    </div>
                    <div className="Contendor-companias">
                        <Company ImgCompany={"/images/icon1.svg"}></Company>
                        <Company ImgCompany={"/images/icon2.svg"}></Company>
                        <Company ImgCompany={"/images/icon3.svg"}></Company>
                    </div>
                </div>       
            </div>
        </section>
    )
}