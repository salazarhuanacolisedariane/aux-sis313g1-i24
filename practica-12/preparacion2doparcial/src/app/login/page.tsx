import Button from "@/components/Button";
import Logo from "@/components/Logo";
import Link from "next/link";
import { Company } from "@/components/Company";

export default function Login() {
    return(
        <section className="login container">
            <div>
                <Logo/>
                <div>
                    <h1>Login</h1>
                    <p>Login to access your travelwise  account</p>
                </div>
                <div>
                    <div>
                        <p>Gmail</p>
                        <input type="text" id="username" name="username" />
                    </div>
                    <div>
                        <p>password</p>
                        <input type="password" id="pass" name="password" />
                        <div className="Remember">
                        <img src="/images/icon-cuadrado.svg" alt="" />
                        <p>Remember Me</p>
                        <span>Forgot Password</span>
                        </div>
                    </div>
                    <div className="login-button">
                        <Button text="Login"></Button>
                    </div>
                    <div className="parte-link">
                            <div>
                            <p>Already have an account?</p>
                            </div>
                            <div>
                            <Link href="/singUp">Sing Up</Link>
                            </div>
                        </div>
                    <div className="Orloginwith">
                        <p>Or login with</p>
                    </div>
                    <div className="Contendor-companias">
                        <Company ImgCompany={"/images/icon1.svg"}></Company>
                        <Company ImgCompany={"/images/icon2.svg"}></Company>
                        <Company ImgCompany={"/images/icon3.svg"}></Company>
                    </div>
                </div>       
            </div>
            <div className="login-img">
                <img src="/images/login.svg"></img>
            </div>
        </section>

    )
}