***
# **_Práctica N°10_**

**Univ.:** Lised Ariane Salazar Huanaco 

**Docente:** Ing. Jose David Mamani Figueroa 

**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

**Siglas:** SIS-313 G1 

**Fecha:** 07/05/2024
***

## **_TAREAS:_**
- [x]  [Link del template]()
- [x]  [Link del proyecto en Figma utilizando el template]()
- [x]  [Link del repositorio creado]()

``` 

               LINK DE LOS TEMPLATE  


```
🔅 [TEMPLATE 1](https://freebiesbug.com/figma-freebies/digital-agency/)

***
🔅 [TEMPLATE 2](https://www.uxcrush.com/figma-gaming-website-template/)
***
```

                  LINK EN FIGMA     
                            
```
🔅 [FIGMA 1](https://www.figma.com/file/f5bQAuKYy2EqwAWPUspfUu/Digital-Agency-(Community)?type=design&node-id=1-2&mode=design&t=jletm05OoBaKt0jy-0) 
***
🔅 [FIGMA 2](https://www.figma.com/file/yTTbjkgXQ92WlNSfvFwu0a/Gaming-Platform---Web-Design-(Community)?type=design&node-id=1-4&mode=design&t=yuwLDPlvDELfgv5x-0) 
***
```

            LINK DEl REPOSITORIO CREADO          

```
***
🔅 [REPOSITORIO CREADO](https://gitlab.com/salazarhuanacolisedariane/react-with-arianesalazar) 
***






