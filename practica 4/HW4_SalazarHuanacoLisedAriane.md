
# **PRÁCTICA N°4**
**UNIV.:** _Lised Ariane Salazar Huanaco_

**MATERIA:** _Diseño y programación gráfica_


## _**GRID ATTACK!!**_

![FONDO](https://github.com/Lisyst/tarea-difici-/assets/152350293/f62112d0-6b05-4699-8b40-cb13cb66cde0)

### _**!NIVELES DIFÍCILES COMPLETADOS¡:**_
- [x]  [level 10]() 
- [x]  [level 18]()
- [x]  [level 20]()
- [x]  [level 29]()
- [x]  [level 30]()
- [x]  [level 40]()
- [x]  [level 45]()
- [x]  [level 50]()
- [x]  [level 60]()
- [x]  [level 66]()
- [x]  [level 70]()
- [x]  [level 78]()
- [x]  [level 80]()


## LEVEL 10!!
![level 10](https://github.com/Lisyst/tarea-difici-/assets/152350293/a6d58770-b80a-445d-b96c-546270c2dd73)


## LEVEL 18!!
![level 18](https://github.com/Lisyst/tarea-difici-/assets/152350293/d6a5fa3e-772f-4fdb-b6f4-1903788d5876)


## LEVEL 20!!
![level 20](https://github.com/Lisyst/tarea-difici-/assets/152350293/5f31f36d-ec69-4b92-bf5f-819b4b75d7a4)


## LEVEL 29!!
![level 29](https://github.com/Lisyst/tarea-difici-/assets/152350293/1dd76ea2-5f6b-43ac-8e3c-753a1dd9579a)


## LEVEL 30!!
![level 30](https://github.com/Lisyst/tarea-difici-/assets/152350293/756c681b-4fa0-40e9-a911-50faf0d471fb)


## LEVEL 40!!
![level 40](https://github.com/Lisyst/tarea-difici-/assets/152350293/d81a5921-22f4-4470-9cfc-b3bee71483cf)


## LEVEL 45!!
![level 45](https://github.com/Lisyst/tarea-difici-/assets/152350293/cd560618-0769-44fb-b553-b457e9af0d4f)


## LEVEL 50!!
![level 50](https://github.com/Lisyst/tarea-difici-/assets/152350293/11541711-af30-4834-aeeb-39bf30025a35)


## LEVEL 60!!
![level 60](https://github.com/Lisyst/tarea-difici-/assets/152350293/044fb6d0-1d87-4c09-9d65-b52a9139cff4)


## LEVEL 66!!
![level 66](https://github.com/Lisyst/tarea-difici-/assets/152350293/8356f23e-2e1b-4b8a-a40d-96e40c0f892a)


## LEVEL 70!!
![level 70](https://github.com/Lisyst/tarea-difici-/assets/152350293/4b740e5f-54f4-4a91-b880-7a87d435eb95)


## LEVEL 78!!
![level 78](https://github.com/Lisyst/tarea-difici-/assets/152350293/03f76f04-d96b-4884-aedc-eb76e2d9208b)


## LEVEL 80!!
![level 80](https://github.com/Lisyst/tarea-difici-/assets/152350293/4a644d36-32e1-4370-88cf-44d163498cc2)