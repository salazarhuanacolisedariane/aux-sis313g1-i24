# _**PRÁCTICA N°7**_ 
***
**Univ.:** Lised Ariane Salazar Huanaco 

**Docente:** Ing. Jose David Mamani Figueroa 

**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA 

**Siglas:** SIS-313 G1 

**Fecha:** 23/04/2024
***
## PREGUNTAS:
**1. Adjunte capturas de pantalla mostrando la versión de node y de npm mediante comandos por la terminal.**

![image](https://github.com/Lisyst/tarea-difici-/assets/152350293/bbb5467e-7b91-4251-aa53-783709eb6e0b)

**2. Realice un video explicando lo siguiente:
a.	La creación de una carpeta en el disco local C: mediante comandos desde la terminal**

**b.	La creación de un proyecto utilizando React con Next.js
NOTA: El video solo deberá explicar la creación de UN SOLO proyecto**

click para ver el video:
https://drive.google.com/drive/folders/1k0bhnenpipP0iiKR1ltntcfJy87RDxkq?hl=es

**3. ¿Qué comandos se utilizó en las anteriores preguntas?**

### cd/
### mkdir (nombre_de_lacarpeta)

### npx create-next-app@latest

### cd (nombre_del_proyecto)

### npm run dev

**4.	¿Qué errores tuvo al crear el proyecto? Tome en cuenta los errores que tuvo al crear el proyecto PARA ESTA PRÁCTICA y para el LABORATORIO DEL DÍA MARTES, 16 DE ABRIL DE 2024.
IMPORTANTE: De preferencia, adjunte las capturas de dichos errores.**

no existe tal archivo o directorio, Istat 'C:\Users\PERSPONAL\AppData\Roaming\npm' 
ósea no existe la carpeta npm 


![image](https://github.com/Lisyst/tarea-difici-/assets/152350293/eef96f48-1aa8-4e93-8d27-313c60992ca2)

Para poder solucionar este problema en windows debemos:

**Paso 1:** abrir el explorador de archivos 

**Paso 2:** buscar la opción de “ver” en la barra 

**Paso 3:** seleccionamos mostrar y le tickeamos elementos ocultos 

**Paso 4:** buscamos el disco local C: 

**Paso 5:** entramos a la carpeta “Roaming” con esta dirección 
C:\Users\PERSPONAL\AppData\Roaming

**Paso 6:** buscamos una carpeta npm en este caso no existe 

**Paso 7:** creamos la carpeta “npm” y con esto debería solucionarse el problema 

**5.	Existen ocasiones, en las en el repositorio (gitlab), una carpeta se torna de color naranja y no se puede acceder al mismo, tal y como se muestra en la imagen.
Este mismo proyecto no permite ser clonado y en caso de éxito en la clonación, la carpeta aparece vacía.
Averigüe la causa del problema y la solución del mismo.**
![image](https://github.com/Lisyst/tarea-difici-/assets/152350293/80972257-54a6-4853-a683-8b229b949050)


## RESPUESTA:
Este error se da cuando en el repositorio existe la carpeta git 
Y existe un git clone dentro de otra carpeta 
En otras palabras existe un repositorio dentro de otro 
![Captura de pantalla (472)](https://github.com/Lisyst/tarea-difici-/assets/152350293/e6ccdfb2-6904-4dc5-8efa-d83b2e9b8022)

La solución sería buscar la carpeta git "clonada" y borrarlo
 




