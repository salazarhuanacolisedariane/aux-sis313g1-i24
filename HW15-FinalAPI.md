

# 🍂 **PRÁCTICA N° 15** 🍂
📌**Univ.:** Lised Ariane Salazar Huanaco

📌**Docente:** Ing. Jose David Mamani Figueroa

📌**Auxiliar:** Univ. Sergio Moises Apaza Caballero 

📌**Materia:** DISEÑO Y PROGRAMACIÓN GRÁFICA

📚**Siglas:** SIS-313 G1     🗓️**Fecha:** 21/06/2024
 
### **PREGUNTAS:** 🔍

**2. Investigar los mensajes de GET, POST y PUT ¿Qué son? ¿Para qué sirven?.........✏️**

### Respuesta:
GET, POST y PUT son métodos de solicitud HTTP utilizados en el desarrollo web para interactuar con servidores.

**GET**

- **Qué es:** El método GET se utiliza para solicitar datos de un servidor.
- **Para qué sirve:** Se usa principalmente para recuperar y consultar información. Las solicitudes GET no deben usarse para operaciones que impliquen cambios en los datos del servidor.
- **Características:**
  - Los parámetros se envían en la URL, generalmente en la cadena de consulta (query string).
  - Es seguro y idempotente, lo que significa que la solicitud no debe cambiar el estado del servidor y puede repetirse sin efectos adicionales.
  - Los datos solicitados se pueden almacenar en caché.

**POST**

- **Qué es:** El método POST se utiliza para enviar datos al servidor para crear o actualizar un recurso.
- **Para qué sirve:** Se usa principalmente para operaciones de creación y actualización de recursos en el servidor, como enviar un formulario, subir un archivo, etc.
- **Características:**
  - Los datos se envían en el cuerpo de la solicitud.
  - No es idempotente, lo que significa que enviar la misma solicitud varias veces puede crear múltiples recursos o tener otros efectos secundarios.
  - Puede enviar grandes cantidades de datos y no está limitado por la longitud de la URL.

**PUT**

- **Qué es:** El método PUT se utiliza para actualizar un recurso existente o crear uno nuevo si no existe.
- **Para qué sirve:** Se usa para actualizar completamente un recurso existente en el servidor. Si el recurso no existe, PUT puede crear uno nuevo en la ubicación especificada.
- **Características:**
  - Los datos se envían en el cuerpo de la solicitud.
  - Es idempotente, lo que significa que realizar la misma solicitud varias veces producirá el mismo resultado (no creará múltiples recursos).
  - Se utiliza para enviar datos completos del recurso que se desea actualizar.

**Resumen:**

- **GET:** Solicita datos.
- **POST:** Envía datos.
- **PUT:** Actualiza datos.

---

**3. ¿Para qué sirven los códigos de respuesta HTTP?.........✏️**

### Respuesta:
Los códigos de respuesta HTTP (HyperText Transfer Protocol) son códigos de estado que el servidor devuelve al cliente (generalmente un navegador web) para indicar el resultado de la solicitud HTTP. Estos códigos ayudan a los desarrolladores y a los navegadores a entender lo que ocurrió con una solicitud específica y a manejar adecuadamente las respuestas.

---

**4. Los códigos de respuesta HTTP se agrupan en 5 clases, investigue cuales son y qué números abarcan..........✏️**

### Respuesta:
Los códigos de respuesta HTTP se agrupan en cinco clases principales, cada una de las cuales abarca un rango específico de números. Aquí están las clases y los números que abarcan:

**1xx (Informativos):**

- **100-199**
  - Indican que la solicitud ha sido recibida y el proceso continúa.

**2xx (Éxito):**

- **200-299**
  - Indican que la solicitud se ha recibido, entendido y aceptado correctamente.

**3xx (Redirección):**

- **300-399**
  - Indican que se deben tomar medidas adicionales por parte del cliente para completar la solicitud.

**4xx (Errores del cliente):**

- **400-499**
  - Indican que hubo un problema con la solicitud realizada por el cliente.

**5xx (Errores del servidor):**

- **500-599**
  - Indican que el servidor falló al cumplir con una solicitud aparentemente válida.

---

**5. Investigar los siguientes códigos de respuesta HTTP, mencione el nombre del código
y explique que indica el mismo..........✏️**

### Respuesta:
#### a. **200 "OK"**
Indica que la solicitud ha tenido éxito.

#### b. **400 "Bad Request"**
La solicitud es incorrecta o tiene un error del cliente.

#### c. **401 "Unauthorized"**
Faltan credenciales de autenticación válidas.

#### d. **403 "Forbidden"**
El cliente no tiene permisos para acceder al recurso.

#### e. **404 "Not Found"**
El recurso solicitado no fue encontrado.

#### f. **408 "Request Timeout"**
La solicitud del cliente tardó demasiado en completarse.

#### g. **500 "Internal Server Error"**
Error inesperado en el servidor.

#### h. **501 "Not Implemented"**
El servidor no reconoce el método de solicitud o no puede cumplirla.

#### i. **502 "Bad Gateway"**
Respuesta inválida del servidor ascendente.

#### j. **504 "Gateway Timeout"**
No se recibió una respuesta a tiempo del servidor ascendente.

---

**6. ¿Qué es un endpoint?..........✏️**

Un **endpoint** es una URL donde un cliente puede acceder a un recurso en un servidor. Es el punto final de comunicación en una red para una API.

---